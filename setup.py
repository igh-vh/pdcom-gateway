#!/usr/bin/env python

# Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
#
# This file is part of the PdCom Gateway.
#
# The PdCom Gateway is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# The PdCom Gateway is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more .
#
# You should have received a copy of the GNU Lesser General Public License
# along with the PdCom Gateway. If not, see <http://www.gnu.org/licenses/>.


from setuptools import setup

setup(
    name="PdCom Gateway",
    version="0.1.1",
    description="PdCom-Based Process Data Gateway",
    author="Bjarne von Horn",
    author_email="vh@igh.de",
    packages=[
        "pdcom_gateway_lib",
        "pdcom_gateway_lib/modules",
        "pdcom_gateway_lib/modules/NMEA",
    ],
    scripts=[
        "bin/pdcom_gateway.py",
    ],
    install_requires=[
        "libconf",
        "pdcom5",
    ],
    python_requires=">=3.6",
)
