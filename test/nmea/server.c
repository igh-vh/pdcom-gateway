/*****************************************************************************
 *
 * Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom Gateway.
 *
 * The PdCom Gateway is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom Gateway is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom Gateway. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#define _XOPEN_SOURCE 600
#include <pdserv.h>
#include <signal.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

volatile sig_atomic_t flag;

static void sighandler(int signum)
{
    (void) signum;
    flag = 1;
}

static int pdservtime(struct timespec *ts)
{
    return clock_gettime(CLOCK_REALTIME, ts);
}

enum {
    signal3_len = 3,
};

struct Task1Data
{
    double wall_time;
    int signal1;
    int signal2;
    unsigned char signal3[signal3_len];
};

struct PdServStuff
{
    struct pdserv *server;
    struct pdtask *task;
    struct pdvariable *wall_signal;
    struct pdvariable *signal1;
    struct pdvariable *signal2;
    struct pdvariable *signal3;
};

static int init_pdserv(struct Task1Data *data, struct PdServStuff *pdservstuff)
{
    pdservstuff->server = pdserv_create("TestServer", "1.0", pdservtime);
    if (!pdservstuff->server)
        return -1;

    pdservstuff->task = pdserv_create_task(pdservstuff->server, 0.1, "Task1");
    if (!pdservstuff->task)
        goto err;

    pdservstuff->wall_signal = pdserv_signal(
            pdservstuff->task, 1, "/lan/World Time", pd_double_T,
            &data->wall_time, 1, NULL);
    if (!pdservstuff->wall_signal)
        goto err;

    pdservstuff->signal1 = pdserv_signal(
            pdservstuff->task, 1, "/lan/signal1", pd_int_T, &data->signal1, 1,
            NULL);
    if (!pdservstuff->signal1)
        goto err;


    pdservstuff->signal2 = pdserv_signal(
            pdservstuff->task, 1, "/lan/signal2", pd_int_T, &data->signal2, 1,
            NULL);
    if (!pdservstuff->signal2)
        goto err;

    pdservstuff->signal3 = pdserv_signal(
            pdservstuff->task, 1, "/lan/signal3", pd_uchar_T, data->signal3,
            signal3_len, NULL);
    if (!pdservstuff->signal3)
        goto err;

    if (pdserv_prepare(pdservstuff->server))
        goto err;

    return 0;
err:
    pdserv_exit(pdservstuff->server);
    return -1;
}

static const long nsec_per_sec = 1000000000;

static void increment(struct timespec *ts)
{
    ts->tv_nsec += 100000000;
    if (ts->tv_nsec >= nsec_per_sec) {
        ts->tv_nsec -= nsec_per_sec;
        ++ts->tv_sec;
    }
}


int main()
{
    struct Task1Data data          = {0};
    struct PdServStuff pdservstuff = {NULL};
    int ret                        = -1;
    struct timespec ts;

    if (signal(SIGTERM, sighandler) == SIG_ERR) {
        return -1;
    }
    if (signal(SIGINT, sighandler) == SIG_ERR) {
        return -1;
    }

    if (init_pdserv(&data, &pdservstuff)) {
        fprintf(stderr, "Could not initialize pdserv\n");
        return -1;
    }

    if (clock_gettime(CLOCK_MONOTONIC, &ts))
        goto done;

    ts.tv_nsec = 0;
    ts.tv_sec += 2;

    while (!flag) {
        if (clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &ts, NULL))
            goto done;
        struct timespec ts2;
        if (clock_gettime(CLOCK_REALTIME, &ts2))
            goto done;

        data.wall_time = ts2.tv_sec + (ts2.tv_nsec * 1.0 / nsec_per_sec);
        data.signal1++;
        data.signal2--;
        for (unsigned int i = 0; i < signal3_len; ++i)
            data.signal3[i] += 2 * (i + 1);

        pdserv_update(pdservstuff.task, &ts2);
        increment(&ts);
    }

    ret = 0;
done:
    pdserv_exit(pdservstuff.server);
    return ret;
}
